<?php
/**
 * Created by PhpStorm.
 * User: Я
 * Date: 27.10.2018
 * Time: 8:31
 */

namespace app\controllers;


use yii\web\Controller;
use app\models\Activity;

class ActivityController extends Controller
{
    public function actionIndex()
    {
        return "Entering controller";
    }

    public function actionCreate()
    {
        $model = new Activity();
        return $this->render('index', [
            'model' => $model,
        ]);
    }
}